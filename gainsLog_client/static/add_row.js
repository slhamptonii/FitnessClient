function addRow(id){
    var table = document.getElementById(id);
    var row = table.insertRow(table.length);

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);

    cell1.innerHTML = '<td><input type="text" name="ex-name" placeholder="name" /></td>';
    cell2.innerHTML = '<td><input type="text" name="st-1-wgt" placeholder="set 1 wgt" size="5"/><input type="text" name="st-1-rps" placeholder="set 1 rps" size="5"/></td>';
    cell3.innerHTML = '<td><input type="text" name="st-2-wgt" placeholder="set 2 wgt" size="5"/><input type="text" name="st-2-rps" placeholder="set 2 rps" size="5"/></td>';
    cell4.innerHTML = '<td><input type="text" name="st-3-wgt" placeholder="set 3 wgt" size="5"/><input type="text" name="st-3-rps" placeholder="set 3 rps" size="5"/></td>';
    cell5.innerHTML = '<td><input type="text" name="st-4-wgt" placeholder="set 4 wgt" size="5"/><input type="text" name="st-4-rps" placeholder="set 4 rps" size="5"/></td>';
    cell6.innerHTML = '<td><input type="text" name="st-5-wgt" placeholder="set 5 wgt" size="5"/><input type="text" name="st-5-rps" placeholder="set 5 rps" size="5"/></td>';
    cell7.innerHTML = '<td><input list="muscle-groups" name="muscle-group"/><datalist id="muscle-groups"><option value="Arms"><option value="Back"><option value="Chest"><option value="Core"><option value="Heart"><option value="Legs"><option value="Shoulders"></datalist></td>';
}