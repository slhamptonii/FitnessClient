from gainsLog_client.model.set import Set


class Exercise:
    name = ""
    muscleGroup = ""
    sets = []

    def __init__(self, name, muscleGroup, sets):
        self.name = name
        self.muscleGroup = muscleGroup
        self.sets = sets

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_muscleGroup(self):
        return self.muscleGroup

    def set_muscleGroup(self, muscleGroup):
        self.muscleGroup = muscleGroup

    def get_sets(self):
        return self.sets

    def set_sets(self, sets):
        self.sets = sets

    def populate_matching_sets(self, total, weight, reps):
        for count in range(total):
            self.sets.apppend(Set(weight, reps))
