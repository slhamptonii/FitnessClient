class User:
    name = None
    password = None
    email = None
    roles = []
    is_authenticated = False
    is_active = True
    is_anonymous = True

    def __init__(self, name, password, email, roles):
        self.name = name
        self.password = password
        self.email = email
        self.roles = roles

    def get_id(self):
        return self.name

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_password(self):
        return self.password

    def set_password(self, password):
        self.password = password

    def get_email(self):
        return self.email

    def set_email(self, email):
        self.email = email

    def get_roles(self):
        return self.roles

    def set_roles(self, roles):
        self.roles = roles
