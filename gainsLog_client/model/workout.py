class Workout:
    name = ""
    calendarName = ""
    userName = ""
    exercises = []

    def __init__(self, name, calendarName, userName, exercises):
        self.name = name
        self.calendarName = calendarName
        self.userName = userName
        self.exercises = exercises

    def get_name(self):
        return self.name

    def set_name(self, name):
        self.name = name

    def get_calendarName(self):
        return self.calendarName

    def set_calendarName(self, calendarName):
        self.calendarName = calendarName

    def get_userName(self):
        return self.userName

    def set_userName(self, userName):
        self.userName = userName

    def get_exercises(self):
        return self.exercises

    def set_exercises(self, exercises):
        self.exercises = exercises
