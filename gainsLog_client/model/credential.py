class Credential:
    eman = ""
    drowssap = ""

    def __init__(self, eman, drowssap):
        self.eman = eman
        self.drowssap = drowssap

    def get_eman(self):
        return self.eman

    def set_eman(self, eman):
        self.eman = eman

    def get_drowssap(self):
        return self.drowssap

    def set_drowssap(self, drowssap):
        self.drowssap = drowssap
