class ResponseHelper:

    isGood = False
    content = None

    def __init__(self, isGood, content):
        self.isGood = isGood
        self.content = content

    def get_isGood(self):
        return self.isGood

    def set_isGood(self, isGood):
        self.isGood = isGood

    def get_content(self):
        return self.content

    def set_content(self, content):
        self.content = content
