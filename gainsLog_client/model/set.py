class Set:
    weight = ""
    reps = ""

    def __init__(self, weight, reps):
        self.weight = weight
        self.reps = reps

    def get_weight(self):
        return self.weight

    def set_weight(self, weight):
        self.weight = weight

    def get_reps(self):
        return self.reps

    def set_reps(self, reps):
        self.reps = reps
