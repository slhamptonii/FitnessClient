import json

import requests

from gainsLog_client.model.response_helper import ResponseHelper


class RequestUtil:

    @staticmethod
    def post(url, data):
        response = requests.post(url, json=data)
        return json.loads(response.content)

    @staticmethod
    def put(url, data):
        response = requests.put(url, data=data)
        return json.loads(response.content)

    @staticmethod
    def get(url):
        response = requests.get(url)
        return json.loads(response.content)

    @staticmethod
    def delete(url):
        response = requests.delete(url)
        return json.loads(response.content)

    @staticmethod
    def handleResponse(response):
        helper = ResponseHelper(isGood=False, content=json.loads(response.content))
        goodResponses = ['200', '201', '202', '203', '204', '205', '206', '207', '208', '226']
        if response.status_code in goodResponses:
            helper.set_isGood(True)

        return helper
