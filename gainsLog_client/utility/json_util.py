import json

import jsonpickle


class JsonUtil:
    @staticmethod
    def obj_to_json_string(obj):
        return jsonpickle.encode(obj, unpicklable=False)

    @staticmethod
    def obj_to_dict(obj):
        json_string = jsonpickle.encode(obj, unpicklable=False)
        return json.loads(json_string)
