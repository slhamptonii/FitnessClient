from gainsLog_client.model.user import User


class UserUtil:

    @staticmethod
    def map_dict_to_user(d):
        return User(d['name'], d['password'], d['email'], d['roles'])
