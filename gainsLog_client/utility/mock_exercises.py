from gainsLog_client.model.exercise import *
from gainsLog_client.model.set import Set
from gainsLog_client.model.workout import Workout


class MockExercises:
    workouts = []

    def __init__(self):
        self.populate_exercises()

    def populate_exercises(self):
        bench_sets = [Set("170", "5"), Set("170", "5"), Set("170", "5"), Set("170", "5"), Set("170", "5")]
        dips_sets = [Set("body", "12"), Set("body", "12"), Set("body", "12"), Set("body", "12"), Set("body", "12")]
        arnolds_sets = [Set("50", "5"), Set("50", "5"), Set("50", "5"), Set("50", "5"), Set("50", "5")]

        arnolds = Exercise("arnolds", "shoulders", arnolds_sets)
        dips = Exercise("dips", "arms", dips_sets)
        bench = Exercise("bench", "chest", bench_sets)
        push_exercises = [bench, dips, arnolds]

        curl_sets = [Set("35", "5"), Set("35", "5"), Set("35", "5"), Set("35", "5"), Set("35", "5")]
        pullup_sets = [Set("body", "6"), Set("body", "6"), Set("body", "6"), Set("body", "6"), Set("body", "6")]
        shrugs_sets = [Set("75", "5"), Set("75", "5"), Set("75", "5"), Set("75", "5"), Set("75", "5")]

        shrugs = Exercise("shrugs", "shoulders", shrugs_sets)
        curl = Exercise("curl", "arms", curl_sets)
        pullup = Exercise("pullup", "chest", pullup_sets)
        pull_exercises = [shrugs, curl, pullup]

        lunges_sets = [Set("50", "5"), Set("50", "5"), Set("50", "5"), Set("50", "5"), Set("50", "5")]
        fsquat_sets = [Set("185", "6"), Set("185", "6"), Set("185", "6"), Set("185", "6"), Set("185", "6")]
        calf_raises_sets = [Set("90", "5"), Set("90", "5"), Set("90", "5"), Set("90", "5"), Set("90", "5")]

        calf_raises = Exercise("calf_raises", "shoulders", calf_raises_sets)
        lunges = Exercise("lunges", "arms", lunges_sets)
        fsquat = Exercise("fsquat", "chest", fsquat_sets)
        legs_exercises = [calf_raises, lunges, fsquat]

        ppl_push = Workout("PPL_PUSH", 0, 0, push_exercises)
        ppl_pull = Workout("PPL_PULL", 0, 0, pull_exercises)
        ppl_legs = Workout("PPL_LEGS", 0, 0, legs_exercises)

        self.workouts.append(ppl_push)
        self.workouts.append(ppl_pull)
        self.workouts.append(ppl_legs)

    def get_workouts(self):
        return self.workouts
