from gainsLog_client.model.credential import *
from gainsLog_client.model.exercise import *
from gainsLog_client.model.set import *
from gainsLog_client.model.user import *
from gainsLog_client.model.workout import *


class FormUtil:
    @staticmethod
    def map_form_to_workout(form, userName):
        workout = Workout(name=None, calendarName=None, userName=None, exercises=[])
        workout.set_userName(userName)
        workout.set_name(form['wk-name'])
        amount = len(form.getlist('ex-name'))

        for x in range(amount):
            ex_name = form.getlist('ex-name')[x]
            ex_muscle_group = form.getlist('muscle-group')[x]
            set1 = Set(form.getlist('st-1-wgt')[x], form.getlist('st-1-rps')[x])
            set2 = Set(form.getlist('st-2-wgt')[x], form.getlist('st-2-rps')[x])
            set3 = Set(form.getlist('st-3-wgt')[x], form.getlist('st-3-rps')[x])
            set4 = Set(form.getlist('st-4-wgt')[x], form.getlist('st-4-rps')[x])
            set5 = Set(form.getlist('st-5-wgt')[x], form.getlist('st-5-rps')[x])
            workout.get_exercises().append(Exercise(ex_name, ex_muscle_group, [set1, set2, set3, set4, set5]))
        return workout

    @staticmethod
    def map_form_to_user(form):
        user = User(name=None, password=None, email=None, roles=[])
        user.set_name(form['user-name'])
        user.set_email(form['user-email'])
        user.set_password(form['user-password'])
        return user

    @staticmethod
    def map_form_to_credential(form):
        credential = Credential(eman=None, drowssap=None)
        credential.set_eman(form['cred-eman'])
        credential.set_drowssap(form['cred-drowssap'])
        return credential
