from flask import Blueprint, render_template, request, current_app
from flask_login import login_required, logout_user, login_user, current_user

from gainsLog_client.utility.form_util import FormUtil
from gainsLog_client.utility.json_util import JsonUtil
from gainsLog_client.utility.request_util import RequestUtil
from gainsLog_client.utility.user_util import UserUtil

mod = Blueprint('home_view', __name__, url_prefix='/')


@mod.route("", methods=["GET"])
def root():
    return render_template("index.html")


@mod.route("home", methods=["GET"])
@login_required
def go_home_roger():
    return render_template("home.html", user=current_user)


@mod.route("login", methods=["POST"])
def login():
    if current_user.is_authenticated:
        return render_template("home.html", user=current_user)
    credential = FormUtil.map_form_to_credential(request.form)
    cred_dict = JsonUtil.obj_to_dict(credential)

    user_d = RequestUtil.post(current_app.config['SERVICE_URL'] + "/login", cred_dict)
    user = UserUtil.map_dict_to_user(user_d)

    user.is_authenticated = True
    user.is_anonymous = False
    user.is_active = True
    login_user(user)

    return render_template("home.html", user=user)


@mod.route("logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return render_template("index.html")
