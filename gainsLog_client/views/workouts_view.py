from flask import Blueprint, render_template, request, current_app
from flask_login import login_required, current_user

from gainsLog_client.utility.form_util import FormUtil
from gainsLog_client.utility.json_util import JsonUtil
from gainsLog_client.utility.request_util import RequestUtil

mod = Blueprint('workouts_view', __name__, url_prefix='/users/<string:userName>/workouts')


@mod.route("/", methods=["GET"])
@login_required
def get_workouts(userName):
    workouts = RequestUtil.get(current_app.config['SERVICE_URL'] + "/users/{}/workouts".format(userName))
    return render_template("workouts.html", user=current_user, workouts=workouts, default_add_size=current_app.config['ADD_SIZE'])


@mod.route("/", methods=["POST"])
@login_required
def create_workout(userName):
    workout = FormUtil.map_form_to_workout(request.form, userName)
    workout_json = JsonUtil.obj_to_dict(workout)
    RequestUtil.post(current_app.config['SERVICE_URL'] + "/users/{}/workouts".format(userName), workout_json)
    return get_workouts(workout.get_userName())


@mod.route("/<string:name>", methods=["GET"])
@login_required
def workout_detail(userName, name):
    workout = RequestUtil.get(current_app.config['SERVICE_URL'] + "/users/{}/workouts/{}".format(userName, name))
    return render_template("workout_detail.html", user=current_user, workout=workout)


@mod.route("/<string:name>", methods=["POST"])
@login_required
def workout_update(userName, name):
    workout = FormUtil.map_form_to_workout(request.form, name)
    workout_json = JsonUtil.obj_to_dict(workout)
    workout = RequestUtil.post(current_app.config['SERVICE_URL'] + "/users/{}/workouts/{}".format(userName, name), workout_json)
    return render_template("workout_detail.html", user=current_user, workout=workout)


@mod.route("/<string:name>", methods=["DELETE"])
@login_required
def workout_delete(userName, name):
    RequestUtil.delete(current_app.config['SERVICE_URL'] + "/users/{}/workouts/{}".format(userName, name))
    return get_workouts()
