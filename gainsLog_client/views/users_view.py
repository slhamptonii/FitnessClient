from flask import Blueprint, render_template, request, current_app
from flask_login import login_required, current_user, login_user, logout_user

from gainsLog_client.utility.form_util import FormUtil
from gainsLog_client.utility.json_util import JsonUtil
from gainsLog_client.utility.request_util import RequestUtil

mod = Blueprint('users_view', __name__, url_prefix='/users')


@mod.route("/<string:userName>", methods=["GET"])
@login_required
def user_detail(userName):
    user = RequestUtil.get(current_app.config['SERVICE_URL'] + "/users/{}".format(userName))
    return render_template("profile.html", user=user)


@mod.route("/", methods=["POST"])
def create_user():
    new_user = FormUtil.map_form_to_user(request.form)
    user_dict = JsonUtil.obj_to_dict(new_user)
    user = RequestUtil.post(current_app.config['SERVICE_URL'] + "/users/", user_dict)

    user.is_authenticated = True
    user.is_anonymous = False
    user.is_active = True
    login_user(user)

    return render_template("home.html", user=current_user)


@mod.route("/<string:userName>", methods=["DELETE"])
@login_required
def delete_user(userName):
    RequestUtil.delete(current_app.config['SERVICE_URL'] + "/users/{}".format(userName))
    logout_user()
    return render_template("index.html")


@mod.route("/<string:userName>", methods=["POST"])
@login_required
def update_user(userName):
    user = FormUtil.map_form_to_user(request.form)
    user_dict = JsonUtil.obj_to_dict(user)
    RequestUtil.post(current_app.config['SERVICE_URL'] + "/users/{}".format(userName), user_dict)
    return render_template("profile.html", user=current_user)
