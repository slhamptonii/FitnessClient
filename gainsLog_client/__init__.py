import logging

from flask import Flask, current_app
from flask_login import LoginManager

from gainsLog_client.utility.request_util import RequestUtil
from gainsLog_client.utility.user_util import UserUtil
from gainsLog_client.views import home_view
from gainsLog_client.views import users_view
from gainsLog_client.views import workouts_view

app = Flask(__name__)
app.config['ADD_SIZE'] = 1
app.config['SERVICE_URL'] = "http://localhost:8080"
app.secret_key = 'This for now'

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_message = 'Login Required'
login_manager.login_view = 'home_view.root'


@login_manager.user_loader
def load_user(user_id):
    logging.info('user_id: {}'.format(user_id))
    user_d = RequestUtil.get(current_app.config['SERVICE_URL'] + "/users/{}".format(user_id))

    user = UserUtil.map_dict_to_user(user_d)

    user.is_authenticated = True
    user.is_anonymous = False
    user.is_active = True
    return user


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r


app.register_blueprint(home_view.mod)
app.register_blueprint(users_view.mod)
app.register_blueprint(workouts_view.mod)


if __name__ == "__main__":
    logging.basicConfig(filename='myapp.log', level=logging.INFO)
