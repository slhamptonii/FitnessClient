from setuptools import setup

setup(
    name='gainsLog_client',
    packages=['gainsLog_client'],
    include_package_data=True,
    install_requires=[
        'flask', 'requests', 'jsonpickle', 'flask_login'
    ],
)